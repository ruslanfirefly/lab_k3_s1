#include <iostream>
#include <cmath>
#include <iostream>

#define DEG_2_RAD(__x__) (__x__ * M_PI / 180.0)

class Point
{
public:

    Point() : x(0), y(0) {}
    Point(float x, float y) : x(x), y(y) {}

    float Length() const
    {
        return sqrt(x*x+y*y);
    }

    Point operator +(const Point& p) const
    {
        return Point(x+p.x, y+p.y);
    }

    Point operator -(const Point& p) const
    {
        return Point(x-p.x, y-p.y);
    }

    Point operator *(const float& s) const
    {
        return Point(x*s, y*s);
    }

    float x;
    float y;
};

class AABB
{
public:

    AABB() {}
    AABB(const Point& a, const Point& b) : a(a), b(b) {}

    friend std::ofstream& operator << (std::ofstream& os, const AABB& aabb);

    Point a;
    Point b;
};

std::ostream& operator << (std::ostream& os, const AABB& aabb)
{
    os << "{ " << aabb.a.x << ", " << aabb.a.y << ", " << aabb.b.x << ", " << aabb.b.y << " }";
    return os;
}

class Shape
{
public:
    virtual float GetArea() const = 0;
    virtual AABB GetAABB() const = 0;
};

class Square : public Shape
{
public:

    Square() : side(0) {}

    void Init(float side)
    {
        this->side = side;
    }

    virtual float GetArea() const
    {
        return side*side;
    }

    virtual AABB GetAABB() const
    {
        return AABB(Point(0, 0), Point(side, side));
    }

    float side;
};

class Diamond : public Shape
{
public:

    void Init(float d1, float d2)
    {
        this->d1 = d1;
        this->d2 = d2;
    }

    virtual float GetArea() const
    {
        return (d1*d2)/2.0f;
    }

    virtual AABB GetAABB() const
    {
        return AABB(Point(0, 0), Point(d1, d2));
    }

    float d1;
    float d2;
};

class Rectandle : public Shape
{
public:

    void Init(const Point& size)
    {
        this->size = size;
    }

    virtual float GetArea() const
    {
        return size.x * size.y;
    }

    virtual AABB GetAABB() const
    {
        return AABB(Point(0, 0), size);
    }

    Point size;
};

class Parallelogram : public Shape
{
public:

    void Init(float a, float b, float alpha)
    {
        this->a = a;
        this->b = b;
        this->alpha = alpha;
    }

    virtual float GetArea() const
    {
        return a*b*sin(alpha);
    }

    virtual AABB GetAABB() const
    {
        AABB aabb;
        aabb.a = Point(0, 0);
        aabb.b = Point(cos(alpha), sin(alpha)) * b;
        aabb.b.x += a;

        return aabb;
    }

    float a;
    float b;
    float alpha;
};

/*
class Triangle : public Shape
{
public:

    void Init(const Point& p1, const Point& p2, const Point& p3)
    {
        this->p1 = p1;
        this->p2 = p2;
        this->p3 = p3;
    }

    virtual float GetArea()
    {
        float a = (p1 - p2).Length();
        float b = (p2 - p3).Length();
        float c = (p3 - p1).Length();

        if(a < (b+c) && b < (a+c) && c < (a+b))
        {
            float p = (a+b+c)/2.0;
            return sqrt(p*(p-a)*(p-b)*(p-c));
        }

        return 0;
    }

    virtual AABB GetAABB()
    {
        AABB aabb;
        aabb.a = Point(std::min(p1.x, p2.x), std::min(p1.y, p2.y));
        aabb.b = Point(std::max(p1.x, p2.x), std::max(p1.y, p2.y));

        aabb.a = Point(std::min(aabb.a.x, p3.x), std::min(aabb.a.y, p3.y));
        aabb.b = Point(std::max(aabb.b.x, p3.x), std::max(aabb.b.y, p3.y));

        return aabb;
    }

    Point p1;
    Point p2;
    Point p3;
};
*/

int main(int argc, char *argv[])
{

    {
        Square shape;
        shape.Init(10);
        std::cout << "Square" << std::endl;
        std::cout << "aabb " << shape.GetAABB() << std::endl;
        std::cout << "area " << shape.GetArea() << std::endl;
    }

    {
        Diamond shape;
        shape.Init(3, 2);
        std::cout << "Diamond" << std::endl;
        std::cout << "aabb " << shape.GetAABB() << std::endl;
        std::cout << "area " << shape.GetArea() << std::endl;
    }

    {
        Rectandle shape;
        shape.Init(Point(10, 5));
        std::cout << "Rectandle" << std::endl;
        std::cout << "aabb " << shape.GetAABB() << std::endl;
        std::cout << "area " << shape.GetArea() << std::endl;
    }

    {
        Parallelogram shape;
        shape.Init(10, 8, DEG_2_RAD(50));
        std::cout << "Parallelogram" << std::endl;
        std::cout << "aabb " << shape.GetAABB() << std::endl;
        std::cout << "area " << shape.GetArea() << std::endl;
    }

    return 0;
}
