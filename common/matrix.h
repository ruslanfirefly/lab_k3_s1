#pragma once

#include "vector.h"

template<class Ty>
class Matrix
{
public:

    typedef Vector<Ty>* data_t;

    Matrix(size_t row, size_t col) : row(row), col(col)
    {
        data = new data_t[row];

        for(size_t r=0; r<row; ++r)
        {
            data[r] = new Vector<Ty>(col);
        }
    }

    Matrix(const Matrix& m) : row(m.row), col(m.col)
    {
        if(this == &m)
            return;

        data = new data_t[row];

        for(size_t r=0; r<row; ++r)
        {
            data[r] = new Vector<Ty>(m[r]);
        }
    }

    virtual ~Matrix()
    {
        for(size_t r=0; r<row; ++r)
        {
            delete data[r];
        }

        delete[] data;
    }

    static Matrix<Ty> create(size_t row, size_t col, ...)
    {
        Matrix<Ty> t(row, col);

        va_list ap;

        va_start(ap, row*col);

        for(size_t r=0; r<row; ++r)
        {
            for(size_t c=0; c<col; ++c)
            {
                t[r][c] = va_arg(ap, Ty);
            }
        }

        va_end(ap);

        return t;
    }

    Vector<Ty>& operator [](size_t r)
    {
        return *(data[r]);
    }

    const Vector<Ty>& operator [](size_t r) const
    {
        return *(data[r]);
    }

    Matrix<Ty> operator + (const Matrix<Ty>& m) const
    {
        Matrix<Ty> t(row, col);

        for(size_t r=0; r<row; ++r)
        {
            t[r] = (*this)[r] + m[r];
        }

        return t;
    }

    Matrix<Ty>& operator += (const Matrix<Ty>& m)
    {
        for(size_t r=0; r<row; ++r)
        {
            (*this)[r] += m[r];
        }

        return *this;
    }

    Matrix<Ty> operator - (const Matrix<Ty>& m) const
    {
        Matrix<Ty> t(row, col);

        for(size_t r=0; r<row; ++r)
        {
            t[r] = (*this)[r] - m[r];
        }

        return t;
    }

    Matrix<Ty>& operator -= (const Matrix<Ty>& m)
    {
        for(size_t r=0; r<row; ++r)
        {
            (*this)[r] -= m[r];
        }

        return *this;
    }

    Matrix<Ty> operator ~()
    {
        Matrix<Ty> t(col, row);

        for(size_t r=0; r<row; ++r)
        {
            for(size_t c=0; c<col; ++c)
            {
                t[c][r] = (*this)[r][c];
            }
        }

        return t;
    }

    Matrix<Ty> operator -()
    {
        Matrix<Ty> t(row, col);

        for(size_t r=0; r<row; ++r)
        {
            t[r] = -(*this)[r];
        }

        return t;
    }

    Matrix<Ty> operator =(const Matrix<Ty>& m)
    {
        if(this == &m)
        {
            return *this;
        }

        Clear();

        row = m.row;
        col = m.col;

        data = new data_t[row];

        for(size_t r=0; r<row; ++r)
        {
            data[r] = new Vector<Ty>(m[r]);
        }

        return *this;
    }

    Matrix<Ty> operator * (const Ty& s) const
    {
        Matrix<Ty> t(row, col);

        for(size_t r=0; r<row; ++r)
        {
            t[r] = (*this)[r] * s;
        }

        return t;
    }

    friend std::ostream& operator << (std::ostream& s, Matrix& m)
    {
        s << "{";

        for(size_t r=0; r<m.row; ++r)
        {
            s << "{";
            for(size_t c=0; c<m.col; ++c)
            {
                s << m[r][c];

                if(c < m.col-1)
                    s << ", ";
            }

            if(r < m.row-1)
                s << "}, ";
            else
                s << "}";
        }

        s << "}";

        return s;
    }

protected:

    void Clear()
    {
        for(size_t r=0; r<row; ++r)
        {
            delete data[r];
        }

        delete[] data;

        data = NULL;
        row = 0;
        col = 0;
    }

    data_t* data;
    size_t row;
    size_t col;
};
