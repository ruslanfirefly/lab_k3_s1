#include <iostream>

#include "../common/matrix.h"

using namespace std;

int main()
{
    Matrix<int> m1(3, 3);

    Matrix<int> m2(Matrix<int>::create(3, 3, 1, 0, 0, 0, 1, 0, 0, 0, 1));

    cout << "m1: " << m1 << endl;
    cout << "m2: " << m2 << endl;

    m1 = m2;
    cout << "m1: " << m1 << endl;

    m1 = m1 * 2;
    cout << "m1: " << m1 << endl;

    cout << "Hello World!" << endl;
    return 0;
}

